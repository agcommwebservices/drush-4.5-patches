## Synopsis

This is a repository that contains tweaked patch files to deal with Drush 4.5's
inability to deal with all patch formats currently being posted to Drupal.org.

## Reasons

It seems Drush 5 and later will attempt 4 methods of applying a patch, while
Drush 4.5 will only attempt 3. Looking at Drush's source we can see that in 5
and later they attempt:

  1. `git apply -p1 file.patch`
  2. `git apply -p0 file.patch`
  3. `patch -p1 --no-backup-if-mismatch -d project_directory < file.patch`
  4. `patch -p0 --no-backup-if-mismatch -d project_directory < file.patch`

Where as in Drush 4.5 they only attempt:

  1. `git apply -p1 file.patch`
  2. `git apply -p0 file.patch`
  3. `patch -p0 --no-backup-if-mismatch -d project_directory < file.patch`

Since the current patch creation instructions are Drupal.org seem to point to
creating patches that require the -p1 flag to be passed to the patch command
the patches will fail to apply in Drush 4.5.

## Modifying patches for Drush 4.5

You will typically have to look at the actual patch involved, but my process to
date has to been to download the exact version of the project indicated and
patch requested by Panopoly. I do this on my local workstation with the
following commands:

  - `drush dl project-7.x-1.0`
  - `curl -LsO http://address/of/file.patch`
   - that is a captial L as Lima, lower case s as in sierra, and a capital
    O as in Oscar.
    - this will create a file in your current directory with the file name
    of the file name part of the web address.
    - the order does not matter.

I then attempt to manually apply the patch with the commands used by Drush 5 and
later in specificed in reasons section above.

Once the patch successfully applies I run the following command to generate a
patch file compatible with Drush 4.5's default -p0 flag and store it in
filename.patch

  - `git diff --no-prefix > filename.patch`

I try to name leave the filename the same as the origianl, just with a suffix
that indicates it was rebuilt.

Finally we add this new patch to our Bitbucket repository so we can reference it
in our Drush Makefiles.

## Referencing the Patch in the Makefile

Since Drush 4.5 only uses the last instance of a defined project we can override
Panopoly's included Makefiles by making sure the last Makefile in the order
has our project specfic overrides. That is where the panopoly_overrides.make
comes into play. Just copy the project you need to override to the corresponding
section of the panopoly_overrides.make file, comment out the original patch by
prepending that line with a semicolon, and then copy that line replacing the URL
with the URL to the RAW view of the file in this repository. **Make sure to
leave the patch number in the reference so that we know what issue this patch
is attempting to solve.**

The reference in panopoly_overrides.make should look something like this:

```
; Panopoly Core - 1.8
projects[fieldable_panels_panes][version] = 1.5
projects[fieldable_panels_panes][subdir] = contrib
;projects[fieldable_panels_panes][patch][2283263] = http://drupal.org/files/issues/fieldable_panels_panes-n2283263-5.patch
projects[fieldable_panels_panes][patch][2283263] = https://bitbucket.org/agcommwebservices/drush-4.5-patches/raw/da55127103b8a78d56bd7cace2ad8b5894d167a2/fieldable_panels_panes-n2283263-5-rebuilt.patch
```

**WATCH OUT**: The hash in that URL is in fact the commit hash for the specific
version of the patch file being referenced. If you update the patch you will
need to make sure to update the references to the patch in your
panopoly_overrides.make file.

## Release Notes

- **29/July/2014**:
    - Repository created.
    - Added .gitignore
    - Added README.md
    - Added fieldable_panels_panes-n2283263-5-rebuilt.patch
    - Added z-index-heart-cools-2050559-1-rebuilt.patch
